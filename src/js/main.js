
(function (JQuery) {
    "use strict";


    /*==================================================================
    [ Validate after type ]*/
    JQuery('.validate-input .input100').each(function () {
        JQuery(this).on('blur', function () {
            if (validate(this) == false) {
                showValidate(this);
            }
            else {
                JQuery(this).parent().addClass('true-validate');
            }
        })
    })


    /*==================================================================
    [ Validate ]*/
    var input = JQuery('.validate-input .input100');

    JQuery('.validate-form').on('submit', function () {
        var check = true;

        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            }
        }

        return check;
    });


    JQuery('.validate-form .input100').each(function () {
        JQuery(this).focus(function () {
            hideValidate(this);
            JQuery(this).parent().removeClass('true-validate');
        });
    });

    function validate(input) {
        if (JQuery(input).attr('type') == 'email' || JQuery(input).attr('name') == 'email') {
            if (JQuery(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if (JQuery(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = JQuery(input).parent();

        JQuery(thisAlert).addClass('alert-validate');

        JQuery(thisAlert).append('<span class="btn-hide-validate">&#xf136;</span>')
        JQuery('.btn-hide-validate').each(function () {
            JQuery(this).on('click', function () {
                hideValidate(this);
            });
        });
    }

    function hideValidate(input) {
        var thisAlert = JQuery(input).parent();
        JQuery(thisAlert).removeClass('alert-validate');
        JQuery(thisAlert).find('.btn-hide-validate').remove();
    }

})(jQuery);