var app = new Vue({
  el: '#app',
  mounted() {
    console.log('Component mounted.')
  },
  data() {
    return {
      name: '',
      description: '',
      output: ''
    };
  },
  methods: {
    formSubmit(e) {
      e.preventDefault();
      let currentObj = this;
      this.axios.post('http://localhost:8000/api/order/new', {
        name: this.name,
        description: this.description
      })
        .then(function (response) {
          currentObj.output = response.data;
        })
        .catch(function (error) {
          currentObj.output = error;
        });
    }
  }
})